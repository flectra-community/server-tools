# Copyright 2020 Ecosoft Co., Ltd (http://ecosoft.co.th/)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html)
{
    "name": "Name Search Multi Lang",
    "summary": "Name search by multiple active language",
    "version": "2.0.1.0.0",
    "category": "Uncategorized",
    "website": "https://gitlab.com/flectra-community/server-tools",
    "author": "Ecosoft, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "depends": ["base"],
    "data": ["views/ir_model_views.xml"],
    "maintainers": ["kittiu"],
    "installable": True,
}

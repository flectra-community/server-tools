# © 2013  Therp BV
# © 2014  ACSONE SA/NV
# Copyright 2018 Quartile Limited
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "dbfilter_from_header",
    "summary": "Filter databases with HTTP headers",
    "version": "2.0.1.0.0",
    "website": "https://gitlab.com/flectra-community/server-tools",
    "author": "Therp BV, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "category": "Tools",
    "depends": ["web"],
    "auto_install": False,
    "installable": True,
}

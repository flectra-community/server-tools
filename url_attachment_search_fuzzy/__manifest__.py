# Copyright 2023 ForgeFlow S.L.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "Fuzzy Search of URL in Attachments",
    "version": "2.0.1.0.0",
    "depends": [
        "base_search_fuzzy",
    ],
    "website": "https://gitlab.com/flectra-community/server-tools",
    "author": "ForgeFlow, Odoo Community Association (OCA)",
    "category": "Tools",
    "license": "AGPL-3",
    "maintainers": ["mariadforgelow"],
    "data": [
        "data/url_attachment_index.xml",
    ],
    "installable": True,
}

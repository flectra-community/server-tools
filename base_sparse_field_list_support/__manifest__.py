{
    "name": "Base Sparse Field List Support",
    "summary": "add list support to convert_to_cache()",
    "version": "2.0.1.0.1",
    "category": "Technical Settings",
    "website": "https://gitlab.com/flectra-community/server-tools",
    "author": "Akretion,Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": ["base", "base_sparse_field"],
}

# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html)
{
    "name": "Model Read Only",
    "category": "Tools",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "Ilyas, ooops404, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/server-tools",
    "maintainers": ["ilyasProgrammer"],
    "depends": ["base"],
    "data": [
        "views/views.xml",
    ],
}

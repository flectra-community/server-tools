{
    "name": "Slow SQL Statement Logger",
    "summary": "Log slow SQL statements",
    "version": "2.0.1.0.1",
    "author": "ACSONE SA/NV, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "website": "https://gitlab.com/flectra-community/server-tools",
    "category": "Tools",
}

# Copyright 2019 Camptocamp SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl)
{
    "name": "Base Many2many Custom Field",
    "summary": "Customizations of Many2many",
    "version": "2.0.1.1.0",
    "category": "Technical Settings",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "website": "https://gitlab.com/flectra-community/server-tools",
    "depends": ["base"],
    "installable": True,
}

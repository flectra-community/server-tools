# Copyright 2023 ACSONE SA/NV
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl).

{
    "name": "Contextvars Patch",
    "summary": """
        Patch Odoo threadlocals to use contextvars instead.""",
    "version": "2.0.1.0.4",
    "license": "LGPL-3",
    "author": "ACSONE SA/NV,Odoo Community Association (OCA)",
    "maintainers": ["sbidoul"],
    "website": "https://gitlab.com/flectra-community/server-tools",
    "external_dependencies": {"python": ["contextvars"]},
}

{
    "name": "Store sessions in DB",
    "version": "2.0.1.0.1",
    "author": "Odoo SA,ACSONE SA/NV,Odoo Community Association (OCA)",
    "license": "LGPL-3",
    "website": "https://gitlab.com/flectra-community/server-tools",
    "maintainers": ["sbidoul"],
}

# Copyright 2019 Trobz <https://trobz.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Bus Alt Connection",
    "summary": "Needed when using PgBouncer as a connection pooler",
    "version": "2.0.1.0.0",
    "author": "Trobz,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/server-tools",
    "category": "Extra Tools",
    "license": "AGPL-3",
    "depends": ["bus"],
    "installable": True,
    "auto_install": False,
}

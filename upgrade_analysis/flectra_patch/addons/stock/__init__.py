# flake8: noqa: B902
from flectra.addons import stock
from ...flectra_patch import OdooPatch


class PreInitHookPatch(OdooPatch):
    target = stock
    method_names = ["pre_init_hook"]

    def pre_init_hook(cr):
        """Don't unlink stock data on reinstall"""

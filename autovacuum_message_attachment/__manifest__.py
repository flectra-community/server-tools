# Copyright (C) 2018 Akretion
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl.html).

{
    "name": "AutoVacuum Mail Message and Attachment",
    "version": "2.0.1.0.0",
    "category": "Tools",
    "website": "https://gitlab.com/flectra-community/server-tools",
    "author": "Akretion, Odoo Community Association (OCA)",
    "license": "LGPL-3",
    "installable": True,
    "summary": "Automatically delete old mail messages and attachments",
    "depends": ["mail"],
    "data": ["data/data.xml", "views/rule_vacuum.xml", "security/ir.model.access.csv"],
}

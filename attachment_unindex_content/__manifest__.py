# © 2019  Vauxoo (<http://www.vauxoo.com/>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Attachment Unindex Content",
    "summary": "Disable indexing of attachments",
    "version": "2.0.1.0.1",
    "author": "Vauxoo, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/server-tools",
    "maintainers": [
        "moylop260",
        "ebirbe",
        "luisg123v",
    ],
    "license": "AGPL-3",
    "category": "Tools",
    "depends": ["base"],
    "installable": True,
    "application": False,
    "post_init_hook": "post_init_hook",
}

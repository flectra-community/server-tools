# Copyright 2021 Quartile Limited
# License LGPL-3.0 or later (http://www.gnu.org/licenses/lgpl).
{
    "name": "Update Restrict Model",
    "version": "2.0.1.1.0",
    "depends": ["base"],
    "website": "https://gitlab.com/flectra-community/server-tools",
    "author": "Odoo Community Association (OCA), Quartile Limited",
    "category": "Others",
    "license": "LGPL-3",
    "data": ["views/ir_model_views.xml", "views/res_users_views.xml"],
    "installable": True,
}

# Flectra Community / server-tools

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[base_name_search_multi_lang](base_name_search_multi_lang/) | 2.0.1.0.0| Name search by multiple active language
[base_remote](base_remote/) | 2.0.1.0.0| Remote Base
[attachment_unindex_content](attachment_unindex_content/) | 2.0.1.0.1| Disable indexing of attachments
[base_sparse_field_list_support](base_sparse_field_list_support/) | 2.0.1.0.1| add list support to convert_to_cache()
[upgrade_analysis](upgrade_analysis/) | 2.0.3.0.0| Performs a difference analysis between modules installed on two different Odoo instances
[auto_backup](auto_backup/) | 2.0.1.0.0| Backups database
[sql_export](sql_export/) | 2.0.1.2.1| Export data in csv file with SQL requests
[jsonifier](jsonifier/) | 2.0.1.1.0| JSON-ify data for all models
[session_db](session_db/) | 2.0.1.0.1| Store sessions in DB
[dbfilter_from_header](dbfilter_from_header/) | 2.0.1.0.0| Filter databases with HTTP headers
[excel_import_export](excel_import_export/) | 2.0.1.1.0| Base module for developing Excel import/export/report
[base_m2m_custom_field](base_m2m_custom_field/) | 2.0.1.1.0| Customizations of Many2many
[fetchmail_notify_error_to_sender_test](fetchmail_notify_error_to_sender_test/) | 2.0.1.0.0| Test for Fetchmail Notify Error to Sender
[excel_import_export_demo](excel_import_export_demo/) | 2.0.1.0.0| Excel Import/Export/Report Demo
[datetime_formatter](datetime_formatter/) | 2.0.1.0.0| Helper functions to give correct format to date[time] fields
[bus_alt_connection](bus_alt_connection/) | 2.0.1.0.0| Needed when using PgBouncer as a connection pooler
[model_read_only](model_read_only/) | 2.0.1.0.0| Model Read Only
[sentry](sentry/) | 2.0.3.0.1| Report Odoo errors to Sentry
[onchange_helper](onchange_helper/) | 2.0.1.0.3| Technical module that ease execution of onchange in Python code
[module_change_auto_install](module_change_auto_install/) | 2.0.1.0.3| Customize auto installables modules by configuration
[base_contextvars](base_contextvars/) | 2.0.1.0.4|         Patch Odoo threadlocals to use contextvars instead.
[attachment_queue](attachment_queue/) | 2.0.1.0.1| Base module adding the concept of queue for processing files
[base_time_window](base_time_window/) | 2.0.1.0.1| Base model to handle time windows
[sql_export_mail](sql_export_mail/) | 2.0.1.0.0| Send csv file generated by sql query by mail.
[module_auto_update](module_auto_update/) | 2.0.1.0.2| Automatically update Odoo modules
[base_time_parameter](base_time_parameter/) | 2.0.3.1.1|         Time dependent parameters        Adds the feature to define parameters        with time based versions.    
[module_analysis](module_analysis/) | 2.0.1.0.0| Test Module for module_analysis
[letsencrypt](letsencrypt/) | 2.0.1.0.2| Request SSL certificates from letsencrypt.org
[base_name_search_improved](base_name_search_improved/) | 2.0.1.1.0| Friendlier search when typing in relation fields
[fetchmail_notify_error_to_sender](fetchmail_notify_error_to_sender/) | 2.0.1.0.0| If fetching mails gives error, send an email to sender
[base_changeset](base_changeset/) | 2.0.2.0.1| Track record changesets
[base_report_auto_create_qweb](base_report_auto_create_qweb/) | 2.0.1.0.1| Report qweb auto generation
[base_technical_user](base_technical_user/) | 2.0.1.0.1|         Add a technical user parameter on the company 
[base_future_response](base_future_response/) | 2.0.1.0.2|         Backport Odoo 16 FutureReponse mechanism.
[base_multi_image](base_multi_image/) | 2.0.1.0.0| Allow multiple images for database objects
[fetchmail_incoming_log](fetchmail_incoming_log/) | 2.0.1.0.0| Log all messages received, before they start to be processed.
[autovacuum_message_attachment](autovacuum_message_attachment/) | 2.0.1.0.0| Automatically delete old mail messages and attachments
[base_custom_info](base_custom_info/) | 2.0.1.0.2| Add custom field in models
[iap_alternative_provider](iap_alternative_provider/) | 2.0.1.0.0| Base module for providing alternative provider for iap apps
[module_prototyper](module_prototyper/) | 2.0.1.0.0| Prototype your module.
[base_exception](base_exception/) | 2.0.2.1.0|     This module provide an abstract model to manage customizable    exceptions to be applied on different models (sale order, invoice, ...)
[sequence_python](sequence_python/) | 2.0.1.0.0| Calculate a sequence number from a Python expression
[auditlog](auditlog/) | 2.0.2.0.1| Audit Log
[slow_statement_logger](slow_statement_logger/) | 2.0.1.0.1| Log slow SQL statements
[html_image_url_extractor](html_image_url_extractor/) | 2.0.1.0.1| Extract images found in any HTML field
[base_kanban_stage](base_kanban_stage/) | 2.0.1.0.0| Provides stage model and abstract logic for inheritance
[base_fontawesome](base_fontawesome/) | 2.0.5.15.4| Up to date Fontawesome resources.
[attachment_synchronize](attachment_synchronize/) | 2.0.1.0.2| Attachment Synchronize
[database_cleanup](database_cleanup/) | 2.0.1.0.2| Database cleanup
[scheduler_error_mailer](scheduler_error_mailer/) | 2.0.1.2.0| Scheduler Error Mailer
[tracking_manager](tracking_manager/) | 2.0.1.0.2| This module tracks all fields of a model,                including one2many and many2many ones.
[rpc_helper](rpc_helper/) | 2.0.1.2.0| Helpers for disabling RPC calls
[sql_export_excel](sql_export_excel/) | 2.0.1.1.0| Allow to export a sql query to an excel file.
[jsonifier_stored](jsonifier_stored/) | 2.0.1.0.0| Pre-compute and store JSON data on any model
[html_text](html_text/) | 2.0.1.0.1| Generate excerpts from any HTML field
[base_jsonify](base_jsonify/) | 2.0.2.0.0|     Base module that provide the jsonify method on all models.    WARNING: since version 14.0.2.0.0 the module have been renamed to `jsonifier`.    This module now depends on it only for backward compatibility.    It will be discarded in v15 likely.    
[base_conditional_image](base_conditional_image/) | 2.0.2.0.0| This module extends the functionality to support conditional images
[base_cron_exclusion](base_cron_exclusion/) | 2.0.1.0.0| Allow you to select scheduled actions that should not run simultaneously.
[test_base_time_window](test_base_time_window/) | 2.0.1.0.1| Test Base model to handle time windows
[base_deterministic_session_gc](base_deterministic_session_gc/) | 2.0.1.0.0| Provide a deterministic session garbage collection instead of the default random one
[base_generate_code](base_generate_code/) | 2.0.1.0.1| Code Generator
[url_attachment_search_fuzzy](url_attachment_search_fuzzy/) | 2.0.1.0.0| Fuzzy Search of URL in Attachments
[nsca_client](nsca_client/) | 2.0.1.0.1| Send passive alerts to monitor your Odoo application.
[base_sequence_default](base_sequence_default/) | 2.0.1.0.0| Use sequences for default values of fields when creating a new record
[base_search_fuzzy](base_search_fuzzy/) | 2.0.1.0.2| Fuzzy search with the PostgreSQL trigram extension
[base_model_restrict_update](base_model_restrict_update/) | 2.0.1.1.0| Update Restrict Model
[sql_request_abstract](sql_request_abstract/) | 2.0.1.3.0| Abstract Model to manage SQL Requests
[configuration_helper](configuration_helper/) | 2.0.1.0.1| Configuration Helper
[base_kanban_stage_state](base_kanban_stage_state/) | 2.0.1.0.0| Maps stages from base_kanban_stage to states
[base_video_link](base_video_link/) | 2.0.1.1.1| Add the possibility to link video on record
[attachment_delete_restrict](attachment_delete_restrict/) | 2.0.1.0.1| Restrict Deletion of Attachments
[mail_cleanup](mail_cleanup/) | 2.0.1.0.0| Mark as read or delete mails after a set time
[base_view_inheritance_extension](base_view_inheritance_extension/) | 2.0.1.1.2| Adds more operators for view inheritance
[base_sequence_option](base_sequence_option/) | 2.0.1.0.0| Alternative sequence options for specific models


